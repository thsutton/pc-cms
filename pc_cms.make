; Drupal and Drush Make versions.
core = 7.x
api = 2

; Default settings.
defaults[projects][subdir] = "contrib"

; Drupal Core
projects[] = drupal

; Utilities
projects[] = ctools
projects[] = entity
projects[] = features
projects[] = libraries
projects[] = pathauto
projects[] = strongarm
projects[] = token
projects[] = views
projects[] = views_bulk_operations

; WYSIWYG editing.
projects[] = wysiwyg
libraries[ckeditor][download][type] = get
libraries[ckeditor][download][url] = http://download.cksource.com/CKEditor/CKEditor/CKEditor%203.6.6.1/ckeditor_3.6.6.1.zip

; File and media support.
projects[] = file_lock
projects[] = file_entity
projects[media][version] = 2.0-alpha3
projects[] = media_youtube

; Administration
projects[] = admin_menu
projects[] = admin_views
projects[] = filter_perms

; Features
projects[pc_media][type] = module
projects[pc_media][subdir] = "features"
projects[pc_media][download][type] = git
projects[pc_media][download][url] = git@bitbucket.org:thsutton/pc-media.git

projects[pc_wysiwyg][type] = module
projects[pc_wysiwyg][subdir] = "features"
projects[pc_wysiwyg][download][type] = git
projects[pc_wysiwyg][download][url] = git@bitbucket.org:thsutton/pc-wysiwyg.git
