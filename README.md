PC CMS
======

[PC CMS][1] is a basic [Drupal][2] installation profile which come
pre-configured with a range of standard functionality.

[1]: https://bitbucket.org/thsutton/pc-cms
[2]: https://drupal.org/

Features
--------

- [PC Media][3] installs and configures the [Media][4] module.

[3]: https://bitbucket.org/thsutton/pc-media
[4]: https://drupal.org/project/media

- [PC WYSIWYG][5] provides rich-text editing using the [WYSIWYG][6] module and
  the [CKEditor][7] library.

[5]: https://bitbucket.org/thsutton/pc-wysiwyg
[6]: https://drupal.org/project/wysiwyg
[7]: http://ckeditor.com

Installation
------------

The quickest and easiest way to install the PC CMS profile is using the
[Drush][8] tool's [Make][9] functionality. Using `drush make` will
automatically download and install all of the modules and libraries this
profile requires.

[8]: http://www.drush.org
[9]: http://www.drush.org/#make

To use `drush make`, create a new text file called `cms.make` with the
following contents:

    core = 7.x
    api = 2
    projects[] = drupal
    projects[pc_cms][type] = profile
    projects[pc_cms][download][type] = git
    projects[pc_cms][download][url] = git@bitbucket.org:thsutton/pc-cms.git

Then use the `drush make` command to build a new Drupal installation based on
that file:

    drush make cms.make my-new-site

This will use the `cms.make` file to make a new Drupal installation in the
`my-new-site` folder. You should probably use something like `htdocs` or
`public_html` instead of `my-new-site`.
